/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicesForClient;

import ServerInterfaces.IUser;
import ServerInterfaces.Status;

/**
 *
 * @author Sara Selim
 */
public class User implements IUser {

    int id;
    String name;
    String country;
    String gender;
    String email;
    Status lastStatus;
    String type;

    public User() {
    }

    public User(int id, String name, String country, String gender, String email, Status lastStatus, String type) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.gender = gender;
        this.email = email;
        this.lastStatus = lastStatus;
        this.type = type;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Status getLastStatus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLastStatus(Status status) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
