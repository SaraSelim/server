/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicesForClient;

import ServerInterfaces.ISignning;
import ServerInterfaces.IUser;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Sara Selim
 */
public class Signning implements ISignning {

    @Override
    public boolean signUp(IUser user) {
        try {
            PreparedStatement pps = DBManager.con.prepareStatement("insert into USERS (id,name,email,gender,contry,type,status) values(?,?,?,?,?,?,?)");
            pps.setInt(1, user.getId());
            pps.setString(2, user.getName());
            pps.setString(3, user.getEmail());
            pps.setString(4, user.getGender());
            pps.setString(5, user.getCountry());
            pps.setString(6, user.getType());
            pps.setString(6, user.getLastStatus().toString());
            pps.close();
            pps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean signOut(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean signIn(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
